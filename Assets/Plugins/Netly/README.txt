NETLY

- Docs
  netly.docs.kezero.com (netly docs)
  github.com/alec1o/byter (byter docs)

- Support
  support@kezero.com (subject: "Netly")

- License
  MIT

- Source code
  github.com/alec1o/netly

- Dependency
  Byter (github.com/alec1o/byter) 

- Powered
  Alecio Furanze: @alec1o (Owner)
  KeZero: @kezerocom (Publisher)