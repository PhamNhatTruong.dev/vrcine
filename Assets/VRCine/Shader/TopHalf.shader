Shader "Custom/SideHalfTexture"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _EmissionTex ("Emission Texture", 2D) = "white" {}
        _EmissionValue("Emission Value",Range (0, 1.5)) = 0.0
        [MaterialToggle]  _Left ("Is Left Side", Float ) = 0
    }
    SubShader
    {
        Tags
        {
            "RenderType"="Opaque"
        }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
 
            #include "UnityCG.cginc"

            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };

            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
            };

            sampler2D _MainTex;
            sampler2D _EmissionTex;
            float _EmissionValue;
            float _Left;
            v2f vert(appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv = v.uv;
                return o;
            }

            fixed4 frag(v2f i) : SV_Target
            {
                bool isGetSide = _Left == 0 ? i.uv.x >= 0.5 : i.uv.x <= 0.5;
                if (isGetSide)
                {
                    fixed4 texColor = tex2D(_MainTex, i.uv);
                    fixed4 emissionColor = tex2D(_EmissionTex, i.uv) * texColor.a * _EmissionValue;
                    return texColor + emissionColor;
                }
                else
                {
                    return fixed4(0, 0, 0, 0);
                }
            }
            ENDCG
        }


    }
    FallBack "Diffuse"
}