using UnityEngine;

namespace VRCine.Scripts.VideoPlayer
{
    public interface IVideoPlayerManager
    {
        void Start();
        void PlayVideo(string name);

        Texture GetVideoBanner(string name);
    }
}