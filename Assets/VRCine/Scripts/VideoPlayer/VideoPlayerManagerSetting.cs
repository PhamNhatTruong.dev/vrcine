using System;
using System.Collections.Generic;
using UnityEngine;

namespace VRCine.Scripts.VideoPlayer
{
    [Serializable]
    [CreateAssetMenu(fileName = "VideoPlayerConfig.asset", menuName = "VRCine/Settings/Video Player Config")]
    public class VideoPlayerManagerSetting : ScriptableObject
    {
        public List<VideoDataSource> Videos;
    }
}
