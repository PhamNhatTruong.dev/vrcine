﻿using System;
using UnityEngine;

namespace VRCine.Scripts.VideoPlayer
{
    [Serializable]
    public class VideoDataSource
    {
        public string VideoName;
        public string VideoPathSource;
        public float DelayTime;
        public Texture VideoBanner;
     }
}