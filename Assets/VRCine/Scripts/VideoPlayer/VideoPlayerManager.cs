﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Events;
using VRCine.Scripts.Signals;
using Zenject;

namespace VRCine.Scripts.VideoPlayer
{
    [UsedImplicitly]
    public class VideoPlayerManager : IVideoPlayerManager
    {
        [Inject] private VideoPlayerManagerSetting _config;
        [Inject] private PKFramework.Logger.ILogger _logger;
        [Inject] private SignalBus _signalBus;
        [Inject] private PKFramework.Runner.IRunner _runner;
        
        private List<VideoDataSource> _videoDataSources;
        
        public void Start()
        {
            _videoDataSources = _config.Videos;
        }

        public void PlayVideo(string name)
        {
            if (_videoDataSources.Any(detail => detail.VideoName.Equals(name)))
            {
                var playedVideo = _videoDataSources.FirstOrDefault(detail => detail.VideoName.Equals(name));
                _signalBus.Fire(new PlayVideoSignal()
                {
                    WaitingTime = playedVideo.DelayTime,
                    VideoSourcePath = playedVideo.VideoPathSource
                });
            }
            else
            {
                _logger.Warning($"Not found video in config : {name}");
            }
        }

    

        public Texture GetVideoBanner(string name)
        {
            if (_videoDataSources.Any(detail => detail.VideoName.Equals(name)))
            {
                var playedVideo = _videoDataSources.FirstOrDefault(detail => detail.VideoName.Equals(name));
                return playedVideo.VideoBanner;
            }
            return new Texture2D(0, 0);
        }
    }
}