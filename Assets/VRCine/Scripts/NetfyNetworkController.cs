// using System.Collections;
// using System.IO;
// using Netly.Core;
// using Netly.Tcp;
// using Netly.Unity;
// using UnityEngine;
// using UnityEngine.Events;
//
// namespace VRCine.Scripts
// {
//     public class NetfyNetworkController : MonoBehaviour
//     {
//         private const string PLAY_VIDEO =  "hit_target";
//     
//         public TcpClient Client;
//         public TcpServer Server;
//
//         private NetlyHost _host;
//         private int _targetIndex;
//         private bool _isClient;
//     
//
//         private void OnEnable()
//         {
//             _host = GetComponent<NetlyHost>();
//          
//             LoadConfig();
//             Init();
//         }
//  
//         private void LoadConfig()
//         {
//             string path = Application.streamingAssetsPath + "/" + "config.txt";
//             if (File.Exists(path))
//             {
//                 var text = File.ReadAllText(path);
//                 var ip = text.Split('/')[0];
//                 _targetIndex =int.Parse( text.Split('/')[1]);
//                 Debug.Log($"Connecting to : '{ip}'");
//                 _host.port = 6128;
//                 _host.ip = ip.Trim();
//             }
//         }
//         // private void pointersPressedHandler(object sender, PointerEventArgs e)
//         // {
//         //     foreach (var pointer in e.Pointers)
//         //     {
//         //         Debug.Log("1 PointersPressed id " + pointer.Id + " pointer type : " + pointer.Type + " touched down at " + pointer.Position);
//         //     }
//         //     if (Client == null || ( Client is {Opened: false} ))
//         //         return;
//         //     var interactPositions = e.Pointers.Select(pointer => new Vector3(pointer.Position.x,pointer.Position.y,0)).ToArray();
//         //     var hitData = new HitTargetData {Hits = interactPositions, SourceIndex = _targetIndex};
//         //
//         //     var hitTargetMessage = JsonUtility.ToJson(hitData);
//         //     Client?.ToEvent(PLAY_VIDEO, Encode.GetBytes(hitTargetMessage));
//         // }
//         
//         private void Init()
//         {
//             _isClient = !_host.isServer;
//
//             #region Client
//
//             if (_isClient)
//             {
//                 Client = new TcpClient();
//                 Client.Open(_host.GetHost());
//                 Client.OnOpen(() =>
//                 {
//                     if (!Client.Opened) return;
//
//                     Client.ToEvent("message", Encode.GetBytes("StartConnect"));
//
//
//                     print("[CLIENT] OnOpen");
//                 });
//
//                 Client.OnError((e) =>
//                 {
//                     print($"[CLIENT] OnError: {e.Message}");
//                     StartCoroutine(DoWaitForSecond(1, Init));
//                 });
//
//                 Client.OnClose(() =>
//                 {
//                     print("[CLIENT] OnClose");
//                     StartCoroutine(DoWaitForSecond(1, Init));
//                 });
//
//                 Client.OnEvent((nameTag, data) =>
//                 {
//                     if (nameTag == PLAY_VIDEO)
//                     {
//                     }
//
//                     print($"[CLIENT] OnEvent ({nameTag}): {Encode.GetString(data)}");
//                 });
//             }
//
//             #endregion
//
//         }
//
//         private IEnumerator DoWaitForSecond(int second , UnityAction onAction)
//         {
//             yield return new WaitForSeconds(second);
//             onAction?.Invoke();
//         }
//     }
// }