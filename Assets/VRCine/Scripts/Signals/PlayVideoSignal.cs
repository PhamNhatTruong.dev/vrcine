﻿using UnityEngine.Video;

namespace VRCine.Scripts.Signals
{
    public class PlayVideoSignal
    {
        public float WaitingTime;
        public string VideoSourcePath;
    }
}