﻿using UnityEngine;

namespace VRCine.Scripts.Signals
{
    public class SetVideoBannerSignal
    {
        public Texture Banner;
    }
}