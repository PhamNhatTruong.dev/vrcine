﻿using System;
using System.Collections;
using Byter;
using Netly;
using Netly.Core;
using UnityEngine;
using UnityEngine.Events;
using VRCine.Scripts.Signals;
using Zenject;
using Network = VRcine.VrCineConstans.Network;

namespace VRCine.Scripts.ClientNetworking
{
    public class ClientNetworking : MonoBehaviour, IClientNetworking
    {
        [Inject] private ClientNetworkingSetting _clientNetworkingSetting;
        [Inject] private PKFramework.Logger.ILogger _logger;
        [Inject] private PKFramework.Runner.IRunner _runner;
        [Inject] private SignalBus _signalBus;

        private Host _host;
        private UdpClient _client;
        private string _guid;
        private bool _isConnected;
        private float _timer;
        
        
        public void StartConnect()
        {
            _isConnected = false;
            _runner.StartCoroutine(DoStartHost(() =>
            {
                _client = new UdpClient();
                _client.OnOpen(() =>
                {
                    _guid = Guid.NewGuid().ToString();
                    _logger.Information($"Client started {_guid}");
                     SendConnectedMess();
                });
                _client.OnClose(() => { _logger.Information("Client closed"); });
                _client.OnError(exception =>
                {
                    _signalBus.Fire(new NotifySignal()
                    {
                        Content = $"Client error {exception.Message}"
                    });
                    _logger.Error($"Client error {exception.Message}");
                });
                _client.OnEvent((s, bytes) =>
                {
                    _logger.Information($"OnEvent {s}");
                    UnityMainThreadDispatcher.Instance().Enqueue(() => { HandlerMessage(s, bytes); });
                });
                _client.Open(_host);
                _client.OnData(bytes =>
                {
                    _logger.Information($"On Data {bytes.Length}");
                    UnityMainThreadDispatcher.Instance().Enqueue(() => { HandlerData(bytes); });

                });
            }));
        }

        IEnumerator DoStartHost(UnityAction onStartCompleted)
        {
            yield return new WaitForSeconds(1.0f);
            try
            {
                _host = new Host(_clientNetworkingSetting.ServerIP, _clientNetworkingSetting.ServerPort);
            }
            catch (Exception e)
            {
                _logger.Error($"Invalid host {e.Message}");
            }

            onStartCompleted?.Invoke();
        }

        private void SendConnectedMess()
        {
            using Writer w = new Writer();
            w.Write(_guid); // ID
            w.Write(true);
            w.Write(Network.ENTER_MESS);
            w.Write(_clientNetworkingSetting.Index.ToString());
            _client.ToEvent(Network.ENTER_MESS, w.GetBytes());
            _client.ToData(w.GetBytes());
            _signalBus.Fire(new NotifySignal()
            {
                Content = "Connect OK"
            });
        }

        private void HandlerMessage(string eventName, byte[] receiveData)
        {
            using Reader r = new Reader(receiveData);
            var id = r.Read<string>(); // ID
            var isClient = r.Read<bool>();
            var name = r.Read<string>();
            var message = r.Read<string>();
            _logger.Information(
                $"Host id {id}: ( is client {isClient} ) receive event from server :({name}):{message}");
            if (eventName.Equals(Network.PLAY_MESS))
            {
                _signalBus.Fire(new RequestVideoSignal()
                {
                    Name = message
                });
            }

            if (eventName.Equals(Network.START_UP))
            {
                _signalBus.Fire(new SetupVideoSignal()
                {
                    Name = message
                });
            }
        }

        private void HandlerData(byte[] receiveData)
        {
            using Reader r = new Reader(receiveData);
            var id = r.Read<string>(); // ID
            var isClient = r.Read<bool>();
            var eventName = r.Read<string>();
            var message = r.Read<string>();
            _logger.Information(
                $"Host id {id}: ( is client {isClient} ) receive event from server :({eventName}):{message}");
            if (eventName.Equals(Network.PLAY_MESS))
            {
                _signalBus.Fire(new RequestVideoSignal()
                {
                    Name = message
                });
            }

            if (eventName.Equals(Network.START_UP))
            {
                _signalBus.Fire(new SetupVideoSignal()
                {
                    Name = message
                });
            }
        }
        public void Ping()
        {
        }
    }
}