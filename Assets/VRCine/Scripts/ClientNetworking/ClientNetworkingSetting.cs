﻿using System;
using UnityEngine;

namespace VRCine.Scripts.ClientNetworking
{
    [Serializable]
    [CreateAssetMenu(fileName = "ClientNetworkingConfig.asset", menuName = "VRCine/Settings/Client networking Config")]
    public class ClientNetworkingSetting : ScriptableObject
    {
        public int DelayTime;
        public int Index;
        public string ServerIP;
        public int ServerPort;
    }
}