namespace VRCine.Scripts.ClientNetworking
{
    public interface IClientNetworking
    {
        void StartConnect();
        void Ping();
    }
}
