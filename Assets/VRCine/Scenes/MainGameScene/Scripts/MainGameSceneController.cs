using System.Collections.Generic;
using DG.Tweening;
using PKFramework.Scene;
using TMPro;
using UnityEngine;
using UnityEngine.Video;
using VRCine.Scripts.Signals;
using Zenject;
#if UNITY_EDITOR
using UnityEditor;
using UnityEditor.SceneManagement;

#endif

// ReSharper disable once CheckNamespace
namespace VRCine.Scenes.MainGameScene
{
    public class MainGameSceneController : SceneController
    {
        [SerializeField] private VideoPlayer _videoPlayer;
        [SerializeField] private List<GameObject> _videoBanner;
        [SerializeField] private GameObject _countdownArea;
        [SerializeField] private TMP_Text _countdownText;
        [SerializeField] private GameObject _debugArea;
        [SerializeField] private TMP_Text _debugText;
        [SerializeField] private float _debugShowTime;
        
        [Inject] private MainGameSceneLogic _logic;
        [Inject] private SignalBus _signalBus;
        [Inject] private PKFramework.Logger.ILogger _logger;

        private Sequence _CountdownSequence;
        private Sequence _showNotifyCanvasSequence;
        //This function call when our scene is being loaded
        public override void OnSceneLoaded(object data)
        {
            RegisterEvent();
            _logic.Start();
            base.OnSceneLoaded(data);
        }

        private void RegisterEvent()
        {
            _signalBus.Subscribe<NotifySignal>(OnReceiveNotify);
            _signalBus.Subscribe<PlayVideoSignal>(OnPlayVideo);
            _signalBus.Subscribe<RequestVideoSignal>(OnRequestPlayVideo);
            _signalBus.Subscribe<SetupVideoSignal>(OnSetupVideo);
            _signalBus.Subscribe<SetVideoBannerSignal>(OnSetVideoBanner);
        }


        private void UnRegisterEvent()
        {
            _signalBus.Unsubscribe<NotifySignal>(OnReceiveNotify);
            _signalBus.Unsubscribe<PlayVideoSignal>(OnPlayVideo);
            _signalBus.Unsubscribe<RequestVideoSignal>(OnRequestPlayVideo);
            _signalBus.Unsubscribe<SetupVideoSignal>(OnSetupVideo);
            _signalBus.Unsubscribe<SetVideoBannerSignal>(OnSetVideoBanner);
        }

        private void OnSetVideoBanner(SetVideoBannerSignal obj)
        {
            _videoPlayer.Stop();
            foreach (var banner in _videoBanner)
            {
                banner.gameObject.SetActive(true);
                var material = banner.GetComponent<MeshRenderer>().material;
                material.mainTexture = obj.Banner;
            }
        }

        private void OnReceiveNotify(NotifySignal obj)
        {
            _debugArea.SetActive(true);
            _showNotifyCanvasSequence?.Kill();
            _showNotifyCanvasSequence = DOTween.Sequence();
            _showNotifyCanvasSequence.AppendInterval(_debugShowTime);
            _showNotifyCanvasSequence.AppendCallback(() =>
            {
                _debugArea.SetActive(false);
                _showNotifyCanvasSequence = null;
             });
        }

        private void OnSetupVideo(SetupVideoSignal obj)
        {
            _countdownArea.SetActive(false);
            _logic.SetUpVideo(obj.Name);
        }

        private void OnRequestPlayVideo(RequestVideoSignal obj)
        {
            _logic.RequestPlayVideo(obj.Name);
            
        }

        private void OnPlayVideo(PlayVideoSignal obj)
        {
            _CountdownSequence = DOTween.Sequence();
            _countdownArea.SetActive(true);
            var tween = DOTween.To(() => 0, x => { _countdownText.text = (obj.WaitingTime - x).ToString("F0"); },
                obj.WaitingTime, obj.WaitingTime).SetEase(Ease.Linear);
            _CountdownSequence.Append(tween);
            _CountdownSequence.AppendCallback(() =>
            {
                _logger.Information("Waiting completed");
                foreach (var banner in _videoBanner)
                {
                    banner.gameObject.SetActive(false);
                }

                _countdownArea.SetActive(false);
                _videoPlayer.Stop();
                _videoPlayer.source = VideoSource.Url;
                _videoPlayer.url = obj.VideoSourcePath;
                _videoPlayer.Play();
            });
            _CountdownSequence.Play();
        }


        //This function call when our scene is being removed/unloaded
        public override void OnSceneUnloaded()
        {
            UnRegisterEvent();
            base.OnSceneUnloaded();
        }


        [ContextMenu("Test")]
        public void PlayTest()
        {
            _logic.Test();
        }


#if UNITY_EDITOR
        [MenuItem("PKFramework/Open Scene/MainGameScene")]
        public static void OpenSceneMainGameScene()
        {
            if (EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo())
            {
                EditorSceneManager.OpenScene(@"Assets/VRCine/Scenes/MainGameScene/Scenes/MainGameScene.unity");
            }
        }
#endif
    }
}