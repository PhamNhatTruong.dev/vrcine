using JetBrains.Annotations;
using PKFramework.Runner;
using PKFramework.Logger;
using VRCine.Scripts.ClientNetworking;
using VRCine.Scripts.Signals;
using VRCine.Scripts.VideoPlayer;
using Zenject;

namespace VRCine.Scenes.MainGameScene
{
    [UsedImplicitly]
    public class MainGameSceneLogic
    {
        [Inject] private ILogger _logger;
        [Inject] private IRunner _runner;
        [Inject] private SignalBus _signalBus;
        [Inject] private IVideoPlayerManager _videoPlayerManager;
        [Inject] private IClientNetworking _clientNetworking;

        public void Start()
        {
            _clientNetworking.StartConnect();
            _videoPlayerManager.Start();
        }

        public void RequestPlayVideo(string name)
        {
            _videoPlayerManager.PlayVideo(name);
        }

        public void SetUpVideo(string name)
        {
            var banner = _videoPlayerManager.GetVideoBanner(name);
            _signalBus.Fire(new SetVideoBannerSignal()
            {
                Banner = banner
            });
        }

        public void Test()
        {
            _videoPlayerManager.PlayVideo("flower");
        }
    }
}