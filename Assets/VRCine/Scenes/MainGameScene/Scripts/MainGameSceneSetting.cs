using System;
using UnityEngine;
using VRCine.Scripts.ClientNetworking;
using VRCine.Scripts.VideoPlayer;
using Zenject;

namespace VRCine.Scenes.MainGameScene
{
    [Serializable]
    [CreateAssetMenu(fileName = "MainGameSceneConfig.asset", menuName = "VRCine/Settings/Main Game Scene Config")]
    public class MainGameSceneSetting : ScriptableObjectInstaller
    {
        [SerializeField] private VideoPlayerManagerSetting _videoPlayerManagerSetting;
        [SerializeField] private ClientNetworkingSetting _clientNetworkingSetting;
        public override void InstallBindings()
        {
            Container.BindInstance(_clientNetworkingSetting);
            Container.BindInstance(_videoPlayerManagerSetting);
        }
    }
}
