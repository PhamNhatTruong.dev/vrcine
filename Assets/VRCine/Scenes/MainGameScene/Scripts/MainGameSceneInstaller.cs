using UnityEngine;
using VRCine.Scripts.ClientNetworking;
using VRCine.Scripts.Signals;
using VRCine.Scripts.VideoPlayer;
using Zenject;

namespace VRCine.Scenes.MainGameScene
{
    public class MainGameSceneInstaller: MonoInstaller
    {
        [SerializeField] private ClientNetworking _clientNetworking;
        
        public override void InstallBindings()
        {
            Container.Bind<MainGameSceneLogic>().AsSingle();
            Container.Bind<IVideoPlayerManager>().To<VideoPlayerManager>().AsSingle();
            Container.Bind<IClientNetworking>().FromComponentInNewPrefab(_clientNetworking).AsSingle();
            SignalBusInstaller.Install(Container);
            Container.DeclareSignal<PlayVideoSignal>();
            Container.DeclareSignal<RequestVideoSignal>();
            Container.DeclareSignal<SetupVideoSignal>();
            Container.DeclareSignal<SetVideoBannerSignal>();
            Container.DeclareSignal<NotifySignal>();
        }
    }
   
}